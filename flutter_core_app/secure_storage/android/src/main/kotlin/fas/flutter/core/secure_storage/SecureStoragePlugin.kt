package fas.flutter.core.secure_storage

import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.util.Log
import androidx.annotation.NonNull
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar

public class SecureStoragePlugin: FlutterPlugin, MethodCallHandler {
  private lateinit var channel : MethodChannel

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.binaryMessenger, CHANNEL)
    context = flutterPluginBinding.applicationContext;
    channel.setMethodCallHandler(SecureStoragePlugin());
  }

  companion object {
    private var context: Context? = null

    const val CHANNEL = "secure_storage"
    const val GET_FUNC = "getFunc"
    const val SET_FUNC = "setFunc"
    const val REMOVE_FUNC = "removeFunc"
    const val CLEAN_UP_FUNC = "cleanUpFunc"

    @JvmStatic
    fun registerWith(registrar: Registrar) {
      val channel = MethodChannel(registrar.messenger(), CHANNEL)
      channel.setMethodCallHandler(SecureStoragePlugin())
    }
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    Log.d("onMethodCall", call.method)
    val options = call.argument<Map<String, String>>("options")
    val uri = options?.get("uri")
    when (call.method) {
      GET_FUNC -> {
        val key = call.argument<String>("key")
        Log.d("onMethodCall", "process get value: key = $key")
        result.success(get(key, uri))
      }
      SET_FUNC -> {
        val key = call.argument<String>("key")
        val value = call.argument<String>("value")
        Log.d("onMethodCall", "process set value: key = $key - value = $value")
        result.success(set(key, value, uri))
      }
      REMOVE_FUNC -> {
        val key = call.argument<String>("key")
        Log.d("onMethodCall", "process remove key: key = $key")
        result.success(remove(key, uri))
      }
      CLEAN_UP_FUNC -> {
        Log.d("onMethodCall", "process clean up")
        result.success(cleanUp(uri))
      }
      else -> result.notImplemented()
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
  }

  private fun get(key: String?, uriStr: String?): String {
    var result = ""

    key?.let {
      if (uriStr != null) {
        // Read preference from other app
        try {
          val uri = Uri.parse(uriStr)
          val cursor = context?.contentResolver?.query(uri, null, key, null, null)
          cursor?.let {
            if (cursor.moveToFirst()) {
              result = cursor.getString(cursor.getColumnIndex(key))
              Log.d("getPreference", "process getValue: result = $result")
            }
            cursor.close()
          }
        } catch (ex: Exception) {
          Log.d("get value error", ex.message)
          return result
        }
      } else {
        // Read internal encrypted preference
        result = try {
          (if (getEncryptedSharedPreferences().contains(key)) {
              getEncryptedSharedPreferences().getString(key, "") ?: ""
          } else {
            Log.d("get value error", "no key $key found")
            return ""
          }).toString()

        } catch (ex: Exception) {
          Log.d("get value error", ex.message)
          return ""
        }
      }
    }

    return result
  }

  private fun set(key: String?, value: String?, uriStr: String?) : Int {
    var result = 0

    key?.let {
      if (uriStr != null) {
        // Write preference from other app
        try {
          val uri = Uri.parse(uriStr)
          val contentValues = ContentValues()
          contentValues.put(key, value)
          result = context?.contentResolver?.update(uri, contentValues, key, null) ?: 0
          Log.d("set status", result.toString())
        } catch (ex: Exception) {
          Log.d("set error", ex.message)
          return result
        }
      } else {
        // Write internal encrypted preference
        try {
          getEncryptedSharedPreferences().edit().putString(key, value).apply()
          result = 1
        } catch (ex: Exception) {
          Log.d("set error", ex.message)
          return result
        }
      }
    }

    return result
  }

  private fun remove(key: String?, uriStr: String?) : Int {
    var result = 0

    key?.let {
      if (uriStr != null) {
        try {
          val uri = Uri.parse(uriStr)
          result = context?.contentResolver?.delete(uri, key, null) ?: 0
          Log.d("remove status: ", result.toString())
        } catch (ex: Exception) {
          Log.d("remove error", ex.message)
          return result
        }
      } else {
        try {
          getEncryptedSharedPreferences().edit().remove(key).apply()
          result = 1
        } catch (ex: Exception) {
          Log.d("remove error", ex.message)
          return result
        }
      }
    }

    return result
  }

  private fun cleanUp(uriStr: String?) : Int {
    var result = 0

    if (uriStr != null) {
      try {
        val uri = Uri.parse(uriStr)
        result = context?.contentResolver?.delete(uri, null, null) ?: 0
        Log.d("remove all  status: ", result.toString())
      } catch (ex: Exception) {
        Log.d("remove all error", ex.message)
        return result
      }
    } else {
      try {
        getEncryptedSharedPreferences().edit().clear().apply()
        result = 1
      } catch (ex: Exception) {
        Log.d("remove all error", ex.message)
        return result
      }
    }

    return result
  }

  private fun getEncryptedSharedPreferences(): SharedPreferences {
    val masterKeyAlias = MasterKey.Builder(context!!.applicationContext).setKeyScheme(MasterKey.KeyScheme.AES256_GCM).build()
    return EncryptedSharedPreferences.create(
            context!!.applicationContext,
            "preferences.secure",
            masterKeyAlias,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
    )
  }
}
