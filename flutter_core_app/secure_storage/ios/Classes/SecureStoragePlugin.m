#import "SecureStoragePlugin.h"
#if __has_include(<secure_storage/secure_storage-Swift.h>)
#import <secure_storage/secure_storage-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "secure_storage-Swift.h"
#endif

static NSString *const kDefaultKeychainService = @"flutter_secure_storage_service";
static NSString *const kInvalidParameters = @"Invalid parameter's type";
static NSString *const kChannelName = @"secure_storage";

@interface SecureStoragePlugin()

@property (strong, nonatomic) NSDictionary *query;
@property (strong) NSString *keychainServiceId;

@end

@implementation SecureStoragePlugin

- (instancetype)init {
  self = [super init];
  if (self) {
    self.keychainServiceId = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"KeychainServiceId"];
    self.query = @{
        (__bridge id)kSecClass :(__bridge id)kSecClassGenericPassword,
        (__bridge id)kSecAttrService : self.keychainServiceId == nil ? kDefaultKeychainService : self.keychainServiceId
    };
  }
  return self;
}

+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  FlutterMethodChannel* channel = [FlutterMethodChannel
                                       methodChannelWithName:kChannelName
                                       binaryMessenger:[registrar messenger]];
  SecureStoragePlugin* instance = [[SecureStoragePlugin alloc] init];
  [registrar addMethodCallDelegate:instance channel:channel];
}


- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
    NSDictionary *arguments = [call arguments];
    NSDictionary *options = [arguments[@"options"] isKindOfClass:[NSDictionary class]] ? arguments[@"options"] : nil;

    if ([@"getFunc" isEqualToString:call.method]) {
        NSString *key = arguments[@"key"];
        NSString *keychainService = options[@"keychainService"];
        NSString *groupId = options[@"groupId"];
        NSString *value = [self read:key forGroup:groupId forService:keychainService];

        result(value);
    } else
    if ([@"setFunc" isEqualToString:call.method]) {
        NSString *key = arguments[@"key"];
        NSString *keychainService = options[@"keychainService"];
        NSString *value = arguments[@"value"];
        NSString *groupId = options[@"groupId"];
        if (![value isKindOfClass:[NSString class]]){
            result(kInvalidParameters);
            return;
        }

        [self write:value forKey:key forGroup:groupId forService:keychainService];

        result(nil);
    } else if ([@"removeFunc" isEqualToString:call.method]) {
        NSString *key = arguments[@"key"];
        NSString *keychainService = options[@"keychainService"];
        NSString *groupId = options[@"groupId"];
        [self delete:key forGroup:groupId forService:keychainService];

        result(nil);
    } else if ([@"cleanUpFunc" isEqualToString:call.method]) {
        NSString *groupId = options[@"groupId"];
        NSString *keychainService = options[@"keychainService"];
        [self deleteAll: groupId forService:keychainService];

        result(nil);
    } else if ([@"getAllFunc" isEqualToString:call.method]) {
        NSString *groupId = options[@"groupId"];
        NSString *keychainService = options[@"keychainService"];
        NSDictionary *value = [self readAll: groupId forService:keychainService];

        result(value);
    }else {
        result(FlutterMethodNotImplemented);
    }
}

- (void)write:(NSString *)value forKey:(NSString *)key forGroup:(NSString *)groupId forService: (NSString *)keychainService {
    NSMutableDictionary *search = [self.query mutableCopy];
    if(groupId != nil) {
        search[(__bridge id)kSecAttrAccessGroup] = groupId;
    }

    if(keychainService != nil) {
        search[(__bridge id)kSecAttrService] = keychainService;
    }

    search[(__bridge id)kSecAttrAccount] = key;
    search[(__bridge id)kSecMatchLimit] = (__bridge id)kSecMatchLimitOne;

    OSStatus status;
    status = SecItemCopyMatching((__bridge CFDictionaryRef)search, NULL);
    if (status == noErr){
        search[(__bridge id)kSecMatchLimit] = nil;

        NSDictionary *update = @{(__bridge id)kSecValueData: [value dataUsingEncoding:NSUTF8StringEncoding]};

        status = SecItemUpdate((__bridge CFDictionaryRef)search, (__bridge CFDictionaryRef)update);
        if (status != noErr){
            NSLog(@"SecItemUpdate status = %d", (int) status);
        }
    }else{
        search[(__bridge id)kSecValueData] = [value dataUsingEncoding:NSUTF8StringEncoding];
        search[(__bridge id)kSecMatchLimit] = nil;

        status = SecItemAdd((__bridge CFDictionaryRef)search, NULL);
        if (status != noErr){
            NSLog(@"SecItemAdd status = %d", (int) status);
        }
    }
}

- (NSString *)read:(NSString *)key forGroup:(NSString *)groupId forService: (NSString *)keychainService {
    NSMutableDictionary *search = [self.query mutableCopy];
    if(groupId != nil) {
     search[(__bridge id)kSecAttrAccessGroup] = groupId;
    }

    if(keychainService != nil) {
        search[(__bridge id)kSecAttrService] = keychainService;
    }

    search[(__bridge id)kSecAttrAccount] = key;
    search[(__bridge id)kSecReturnData] = (__bridge id)kCFBooleanTrue;

    CFDataRef resultData = NULL;

    OSStatus status;
    status = SecItemCopyMatching((__bridge CFDictionaryRef)search, (CFTypeRef*)&resultData);
    NSString *value;
    if (status == noErr){
        NSData *data = (__bridge NSData*)resultData;
        value = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    }

    return value;
}

- (void)delete:(NSString *)key forGroup:(NSString *)groupId forService: (NSString *)keychainService {
    NSMutableDictionary *search = [self.query mutableCopy];
    if(groupId != nil) {
        search[(__bridge id)kSecAttrAccessGroup] = groupId;
    }

    if(keychainService != nil) {
        search[(__bridge id)kSecAttrService] = keychainService;
    }

    search[(__bridge id)kSecAttrAccount] = key;
    search[(__bridge id)kSecReturnData] = (__bridge id)kCFBooleanTrue;

    SecItemDelete((__bridge CFDictionaryRef)search);
}

- (void)deleteAll:(NSString *)groupId forService: (NSString *)keychainService {
    NSMutableDictionary *search = [self.query mutableCopy];
    if(groupId != nil) {
        search[(__bridge id)kSecAttrAccessGroup] = groupId;
    }

    if(keychainService != nil) {
        search[(__bridge id)kSecAttrService] = keychainService;
    }

    SecItemDelete((__bridge CFDictionaryRef)search);
}

- (NSDictionary *)readAll:(NSString *)groupId forService: (NSString *)keychainService {
    NSMutableDictionary *search = [self.query mutableCopy];
    if(groupId != nil) {
        search[(__bridge id)kSecAttrAccessGroup] = groupId;
    }

    if(keychainService != nil) {
        search[(__bridge id)kSecAttrService] = keychainService;
    }

    search[(__bridge id)kSecReturnData] = (__bridge id)kCFBooleanTrue;

    search[(__bridge id)kSecMatchLimit] = (__bridge id)kSecMatchLimitAll;
    search[(__bridge id)kSecReturnAttributes] = (__bridge id)kCFBooleanTrue;

    CFArrayRef resultData = NULL;

    OSStatus status;
    status = SecItemCopyMatching((__bridge CFDictionaryRef)search, (CFTypeRef*)&resultData);
    if (status == noErr){
        NSArray *items = (__bridge NSArray*)resultData;

        NSMutableDictionary *results = [[NSMutableDictionary alloc] init];
        for (NSDictionary *item in items){
            NSString *key = item[(__bridge NSString *)kSecAttrAccount];
            NSString *value = [[NSString alloc] initWithData:item[(__bridge NSString *)kSecValueData] encoding:NSUTF8StringEncoding];
            results[key] = value;
        }
        return [results copy];
    }

    return @{};
}
@end
